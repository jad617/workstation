# Workstation i3-gaps

Automate i3-gaps deployment on Ubuntu 18.04 or higher

### Prerequisites

```
ansible>=2.5
python-pip

#If running on a remote workstation, you will need to add your ssh key to root user
```

## Getting Started

* If you do not wish to create users, you can comment the "create_users" role in playbook-workstation_setup.yaml 

```
---

- hosts: all
  gather_facts: false
  become: yes
  roles:
#    - create_users
    - dotfiles
    - i3-gaps
    - terminal
    - polybar
    - autorandr
    - rofi
    - vimrc

```

You will also need to modify the default_group value in group_var/all.yaml
<br>
It will need to match the group of your current user
```
---

default_group: <EXISTING_USER_GROUP>
```


* If you decide to create user(s).
I have added in the user variable in the  group_var/all.yaml

```
default_user: USER
default_user_pass: UNIX_PASSWORD_HASH

default_group: DEFAULT_GROUP

And replace them by a User, Password HASH and a default group:
#Example:

```

* Example
```
---

default_user: testuser
default_user_pass: $6$GRCalE0.Y9msasSzM$t/8/gTKKaJe1KrCShZNXg0VvduxgJrt/aZayIoJpwP2bPBEUX/dGAGglVNvrE62otM3b1VbzxC

default_group: testgroup

users:
  - name: "{{ default_user }}"
    group: "{{ default_group }}"
    pass: "{{ default_user_pass }}"

```
* To add 1 or multiple extra users
```
users:
  - name: "{{ default_user }}"
    group: "{{ default_group }}"
    pass: "{{ default_user_pass }}"

  - name: second_user
    group: "{{ default_group }}" #Kept the same group for simplicity
    pass: "{{ default_user_pass }}" #Kept the same password hash for simplicity

  - name: third_user
    group: "{{ default_group }}" #Kept the same group for simplicity
    pass: "{{ default_user_pass }}" #Kept the same password hash for simplicity

  - name: third_user
    group: "{{ default_group }}" #Kept the same group for simplicity
    pass: "{{ default_user_pass }}" #Kept the same password hash for simplicity
```



### Configuring the inventory

Before running the playbook, make sure that you have the host configured in the inventory/hosts fils

* For remote workstation 
```
[workstation]
##Or for remote workstation
vmi-3 ansible_host=<LOCAL_IP>
```
OR

```
[workstation]
##Example for a VM with port forwarding
127.0.0.1 ansible_port=<PORT>
```

* For local workstation 
```
[local_workstation]
#Example for running ansible on your own workstation
127.0.0.1 ansible_connection=local
```

### Running the playbook
```
#Running on a remote workstation
ansible-playbook playbook-workstation_setup.yaml --limit "vm-i3*"

#Running locally
sudo ansible-playbook playbook-workstation_setup.yaml --limit "workstation_local*"
```

## Result
![Default](i3-gaps.png "Default")
![Busy](i3-gaps2.png "Busy")
